import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';

import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'

class Check extends Component{

    constructor(){
        super()
        this.state = {
            text: ''
        }
        this.onSelect = this.onSelect.bind(this)
    }

    onSelect(index, value){
        this.setState({
        text: `${value}`
        })
    }

    getGen () {
        return this.state.text
    }

    render(){
        return(
            <View style={styles.container}>
                <RadioGroup 
                    onSelect = {(index, value) => this.onSelect(index, value)}  
                >
                    <RadioButton value={'male'} >
                        <Text>Male</Text>
                    </RadioButton>
                    <RadioButton value={'female'}>
                        <Text>Female</Text>
                    </RadioButton>

                </RadioGroup>
            </View>
        )
    }
}

let styles = StyleSheet.create({
    container: {
        marginTop: 5,
        padding: 20
    },
    text: {
        padding: 10,
        fontSize: 14,
    },
})

module.exports = Check
