import React, { Component } from 'react';
import { View, Text, Picker, StyleSheet } from 'react-native'

class DropboxTime extends Component {
    constructor () {
        super()
        this.state = {
            time: 'shortDay'
        }
    }
    getTime () {
        return this.state.time
    }

    updateUser (time) {
        this.setState({ time: time })
    }
   render() {
      return (
         <View>
            <Text style={styles.cardSectionStyle}>Duration</Text>
            <Picker selectedValue = {this.state.time} onValueChange = {(itemValue) => this.updateUser(itemValue)}>
               <Picker.Item label = "1 - 3 Days" value = "shortDay" />
               <Picker.Item label = "4 - 6 Days" value = "longDay" />
               <Picker.Item label = "2 Weeks" value = "week" />
               <Picker.Item label = "More 2 Weeks" value = "mweek" />
            </Picker>
            {/* <Text style = {styles.text}>{this.state.user}</Text> */}
         </View>
      )
   }
}

export default DropboxTime

const styles = StyleSheet.create({
   text: {
      fontSize: 30,
      alignSelf: 'center',
      color: 'red'
   },
   textStyle: {
       fontSize: 20,
       color: '#FFF',
       fontWeight: 'bold',  
       marginBottom: 5      
   }
})