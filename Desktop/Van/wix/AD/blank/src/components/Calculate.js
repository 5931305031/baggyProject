import React, { Component } from 'react';
import { View, Text, Button, TextInput, StyleSheet, Alert, TouchableOpacity } from 'react-native';
import { StackNavigator } from 'react-navigation';

class Calculate extends Component {
    
    state = {
        days: ''
    }
    handleDays = (text) => {
        this.setState({ days: text })
    }
    render() {
        return (
            <View style={styles.container}>
                <TextInput style={styles.input}
                    underlineColorAndroid="transparent"
                    placeholder="Days"
                    placeholderTextColor="#9a73ef"
                    autoCapitalize="none"
                    onChangeText={this.handleDays} />

                <Button
                    title = 'Click'
                    style={styles.submitButton}
                    onPress={ this.Simple_If}
                />
            </View>
        );
    }

    Simple_If = () => {
        if(this.state.days == 2){
            <TouchableOpacity onPress={this.navigionTo}>
                
            </TouchableOpacity>
        }else{
            Alert.alert('OTHER'); 
        }
    }

    navigionTo = () => {
        this.props.navigation.navigate('Test');
    }
}



export default Calculate;

const styles = StyleSheet.create({
    container: {
        paddingTop: 23
    },
    input: {
        margin: 15,
        height: 40,
        borderColor: '#7a42f4',
        borderWidth: 1
    },
    submitButton: {
        backgroundColor: '#7a42f4',
        padding: 10,
        margin: 15,
        height: 40,
    },
    submitButtonText: {
        color: 'white'
    }
});

