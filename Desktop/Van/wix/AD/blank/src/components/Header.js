import React from 'react';
import {View, Text} from 'react-native';

const Header = (props) => {
    return (
            <View style={styles.headerStyle}>
                <Text style={styles.textStyle}>{props.name}</Text>
            </View>
    );
};

const styles = {
    headerStyle: {
        height: 60,
        backgroundColor: 'orange',
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.2,
        elevation: 2,
        position: 'relative'
    },
    textStyle: {
        fontSize: 20,
        color: '#FFF',
        fontWeight: 'bold'
    }
};

export default Header;


