import React, { Component } from 'react';
import { View, Text, Picker, StyleSheet, Alert } from 'react-native'

class Dropbox extends Component {
    constructor () {
        super()
        this.state = {
            bag: 'backpack'
        }
    }
    getBag () {
        return this.state.bag
    }

    updateUser (bag) {
        this.setState({ bag: bag })
    }
    render() {
        return (
            <View>
                <Text style={styles.cardSectionStyle}>Types of bags</Text>
                <Picker selectedValue={this.state.bag} onValueChange={(itemValue) => this.updateUser(itemValue)}>
                    <Picker.Item label="Backpack" value="backpack" />
                    <Picker.Item label="Baggage" value="baggage" />
                </Picker>
                {/* <Text style={styles.text}>{this.state.user}</Text> */}
            </View>
        )
    }
}
export default Dropbox

const styles = StyleSheet.create({
    text: {
        fontSize: 30,
        alignSelf: 'center',
        color: 'red'
    },
    textStyle: {
        fontSize: 20,
        color: '#FFF',
        fontWeight: 'bold',  
        marginBottom: 5,
        marginLeft: 10,
        marginTop: 10   
    }
})