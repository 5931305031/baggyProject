import React from 'react';
import {View, Text} from 'react-native';
// import Card from "./Card";

const CardSection = () => {
    return (
        <View style={styles.cardSectionStyle}>
            <Text>Item1</Text>
            <Text>Item2</Text>
        </View>
    );
};

const styles = {
    cardSectionStyle: {
        flexDirection: 'row',
        padding: 5,
        borderColor: '#ddd',
        borderWidth: 1,
        position: 'relative'
    }
};

export default CardSection;