import React, { Component } from 'react';
import { View, Text, Button, StyleSheet, Alert } from 'react-native';
import Header from './src/components/Header';
// import AlbamDetail from './src/componentss/AlbamDetail';
// import Calculate from './src/components/Calculate';
import Dropbox from './src/components/Dropbox';
import DropboxTime from './src/components/DropboxTime';
import Check from './src/components/Check';
import ListView from './src/components/ListView';

// const App = () => {
//   return (
//     <View style={{ flex: 1 }}>
//       <Header name="Albam" />
//       <Dropbox />
//       <DropboxTime />
//       <Check />
//       <Button
//         title='Click'
//         onPress={this.SelectCond}
//       />
//     </View>
//   );
// };

class App extends Component {

  state = {
    days: ''
  }
  handleDays = (text) => {
    this.setState({ days: text })
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header name="Albam" />
        <Dropbox
          ref="bag"
        />
        <DropboxTime
          ref="time"
        />
        <Check
          ref="gen"
        />
        {/* <Button
          title='get bag'
          onPress={() => { alert(this.refs.gen.getGen()) }}
        /> */}
        <Button
          title='Click'
          onPress={this.SelectCond}
        />
      </View>
    );
  }

  SelectCond = () => {
    if (this.refs.bag.getBag() === 'backpack') {
      if (this.refs.gen.getGen() === 'male') {
        if (this.refs.time.getTime() === 'shortDay') {
          // Alert.alert('Hello Male 1-3')
          <ListView/>
        } else if (this.refs.time.getTime() === 'longDay') {
          Alert.alert('Hello Male 4-6')
        } else if (this.refs.time.getTime() === 'week') {
          Alert.alert('Hello Male Week')
        } else {
          Alert.alert('Hello Male More Week')
        }
      } else if (this.refs.gen.getGen() === 'female') {
        if (this.refs.time.getTime() === 'shortDay') {
          Alert.alert('Hello Female 1-3')
        } else if (this.refs.time.getTime() === 'longDay') {
          Alert.alert('Hello Female 4-6')
        } else if (this.refs.time.getTime() === 'week') {
          Alert.alert('Hello Female Week')
        } else {
          Alert.alert('Hello Female More Week')
        }
      } else {
        Alert.alert('Error')
      }
    } else if (this.refs.bag.getBag() === 'baggage') {
      if (this.refs.gen.getGen() === 'male') {
        if (this.refs.time.getTime() === 'shortDay') {
          Alert.alert('Bello Male 1-3')
        } else if (this.refs.time.getTime() === 'longDay') {
          Alert.alert('Bello Male 4-6')
        } else if (this.refs.time.getTime() === 'week') {
          Alert.alert('Bello Male Week')
        } else {
          Alert.alert('Bello Male More Week')
        }
      } else if (this.refs.gen.getGen() === 'female') {
        if (this.refs.time.getTime() === 'shortDay') {
          Alert.alert('Bello Female 1-3')
        } else if (this.refs.time.getTime() === 'longDay') {
          Alert.alert('Bello Female 4-6')
        } else if (this.refs.time.getTime() === 'week') {
          Alert.alert('Bello Female Week')
        } else {
          Alert.alert('Bello Female More Week')
        }
      } else {
        Alert.alert('Error')
      }
    } else {
      Alert.alert('Wrong')
    }
  }

  navigionTo = () => {
    this.props.navigation.navigate('Test');
  }
}

export default App;

const styles = StyleSheet.create({
  container: {
    paddingTop: 23
  },
  input: {
    margin: 15,
    height: 40,
    borderColor: '#7a42f4',
    borderWidth: 1
  },
  submitButton: {
    backgroundColor: '#7a42f4',
    padding: 10,
    margin: 15,
    height: 40,
  },
  submitButtonText: {
    color: 'white'
  }
});
